cd ..
cd .ssh
(
echo # Home Account
echo Host gitlab.com
echo        HostName gitlab.com
echo        PreferredAuthentications publickey
echo        IdentityFile ~/.ssh/personal

echo # Company Account
echo Host company.gitlab.com
echo        HostName company.gitlab.com
echo        PreferredAuthentications publickey
echo        IdentityFile ~/.ssh/company1

echo # Company Account
echo Host company.github.com
echo        HostName  company.github.com
echo        PreferredAuthentications publickey
echo        IdentityFile ~/.ssh/company2
)>> config